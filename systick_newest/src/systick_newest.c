/*
===============================================================================
 Name        : systick_newest.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
#include "gpio_17xx_40xx.h"

#define PORT 0
#define RED_LED 22

uint32_t to_mili(uint32_t time);
void SysTick_Handler(void);


int main(void) {
	SystemInit();
	SystemCoreClockUpdate();
	SysTick_Config(to_mili(10));
	Chip_GPIO_Init(LPC_GPIO);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,0,22);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, PORT, RED_LED);
    while(1) {
    }
    return 0 ;
}

void SysTick_Handler(void){
	Chip_GPIO_SetPinToggle(LPC_GPIO, PORT, RED_LED);
}


uint32_t to_mili(uint32_t time){
	return (SystemCoreClock/1000)*time;
}
