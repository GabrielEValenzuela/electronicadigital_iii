# (7420) ElectronicaDigital_III

Código fuente usado durante exposiciones teóricas.

## How to en simple pasos
Primero, debemos bajar el repositorio

![imagen1](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img1.png)

Posteriormente descomprimimos en y extraemos los archivos en el **workspace** de nuestro MCU.
Una vez hecho esto, vamos a linkear de tal forma que nos quede *(USAREMOS LOS EJEMPLOS DE SYSTICK)*

### Si queremos usar CMSIS debemos tener configurado nuestro proyecto de esta forma


![imagen2](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img2.png)


![imagen3](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img3.png)

## Si queremos usar LPCOpen debemos tener configurado nuestro proyecto de esta forma

![imagen4](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img4.png)

![imagen5](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img5.png)
</br>

# :warning: Debemos compilar los proyectos [lpcopen](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/tree/master/lpc_chip_175x_6x) y [cmsis](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/tree/master/CMSISv2p00_LPC17xx) antes de utilizarlos.
# ¿Problemas?

Dirijite al panel izquierdo -> issue:

![imagen6](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img6.png)

Una vez dentro, hace click en **New issue**

![imagen7](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img7.png)

Describí tu problema, adjunta capturas para ayudar y estate atento a las respuestas

![imagen8](https://gitlab.com/GabrielEValenzuela/electronicadigital_iii/-/raw/doc/doc/img8.png)
