/*
===============================================================================
 Name        : systick_old.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
 */

#ifdef __USE_CMSIS
#include <lpc17xx.h>
#endif

#include <cr_section_macros.h>
#include "lpc17xx_systick.h"


#define STCTRL      (*( ( volatile unsigned long *) 0xE000E010 )) // System Timer Control and status register
#define STRELOAD    (*( ( volatile unsigned long *) 0xE000E014 )) // System Timer Reload value register
#define STCURR      (*( ( volatile unsigned long *) 0xE000E018 )) // System Timer Current value register

#define SBIT_ENABLE 0 //System Tick counter enable. When 1, the counter is enabled. When 0,
					  //the counter is disabled.
#define SBIT_TICKINT 1 //System Tick interrupt enable. When 1, the System Tick interrupt is
					   //enabled. When 0, the System Tick interrupt is disabled. When enabled,
					   //the interrupt is generated when the System Tick counter counts down to 0.
#define SBIT_CLKSOURCE 2 //System Tick clock source selection. When 1, the CPU clock is selected.
				         //When 0, the external clock pin (STCLK) is selected

#define RELOAD_VALUE 9999
/*!
 * 100MHz * 1ms = 100000 - 1
 */

#define OUT_LED 22 //P0.22



int main(void) {

	SystemInit();
	STRELOAD = RELOAD_VALUE;

	STCTRL = (1<<SBIT_ENABLE) | (1<<SBIT_TICKINT) | (1<<SBIT_CLKSOURCE);

	LPC_GPIO0->FIODIR |= (1<<OUT_LED);

	while(1) {
	}
	return 0 ;
}

void SysTick_Handler(void){
	LPC_GPIO0->FIODIR ^= (1<<OUT_LED);
}
