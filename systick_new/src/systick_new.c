/*
===============================================================================
 Name        : systick_old.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
 */

#ifdef __USE_CMSIS
#include <lpc17xx.h>
#endif

#include <cr_section_macros.h>
#include "lpc17xx_systick.h"
#include "lpc17xx_gpio.h"

#define PORT 0
#define PIN_SELECT 22
#define PORT_ZERO	(uint32_t) 0

#define OUTPUT	(uint8_t) 1

#define RED_LED		( (uint32_t) (1<<22) )


int main(void) {

	SystemInit();
	PINSEL_CFG_Type pin_config; //Estrucutura de configuración de pin

	pin_config.Portnum = PINSEL_PORT_0;
	pin_config.Pinnum  = PINSEL_PIN_22;
	pin_config.Pinmode = PINSEL_PINMODE_PULLUP;
	pin_config.Funcnum = PINSEL_FUNC_0;
	pin_config.OpenDrain = PINSEL_PINMODE_NORMAL;

	PINSEL_ConfigPin(&pin_config);
	GPIO_SetDir(PORT_ZERO, RED_LED, OUTPUT);

	SYSTICK_InternalInit(10); //Systick de 10ms

	SYSTICK_IntCmd(ENABLE); //Habilita interrupciones
	SYSTICK_Cmd(ENABLE); //Comienza el systick

	while(1) {
	}
	return 0 ;
}

void SysTick_Handler(void){
	SYSTICK_ClearCounterFlag(); //Limpia bandera de interrupción
	if(GPIO_ReadValue(PORT)&PIN_SELECT){
		GPIO_SetValue(PORT_ZERO, RED_LED);
	}
	else{
		GPIO_ClearValue(PORT_ZERO, RED_LED);
	}
}
