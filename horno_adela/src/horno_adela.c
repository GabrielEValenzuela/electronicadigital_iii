/*
===============================================================================
 Name        : Horno Adela S.A
 Author      : Gabriel Valenzuela
 Version     : 1.0.0.
 Copyright   : MIT Licence
 Description : @ToDo: Implementar encendido/apagado de leds, reseteo de timer 0 y dividir funcion iniciar_sistema()
===============================================================================
 */

#ifdef __USE_CMSIS
#include <lpc17xx.h>
#endif

#include <cr_section_macros.h>
#include "lpc17xx_adc.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_timer.h"

#define REDLED 		( (uint32_t) (1<<18) )
#define YELLOWLED 	( (uint32_t) (1<<17) )
#define GREENLED 	( (uint32_t) (1<<16) )
#define ADC_MAX_FREQ 200000
#define PORT_ZERO ((uint8_t) 0)
#define OUTPUT ((uint8_t) 1)
#define CHANNEL_ZERO ((uint8_t) 0)
#define SECOND 	(uint32_t)	10000
#define COUNT_CONVERTION ((uint32_t) 10)

//LED ROJO - P0.18
//LED AMARILLO - P0.17
//LED VERDE - P0.16
//ANALOG_INPUT - P0.23

/*!
 * Configuracion inicial del sistema:
 * 	- Configurar Timer0
 * 	- Configurar entrada analógica
 * 	- Configurar salidas digitales
 * 	- Configurar ADC
 */
void iniciar_sistema();

/*!
 * Comenzar la cuenta de timer 0
 */
void encender_timer0();

/*!
 * Analizar el resultado de conversión
 */

void analizar_resultado(uint32_t valor_medido);

/*!
 * Sobre escritura de la funcion de handler de interrupcion del ADC
 */
void ADC_IRQHandler();

/*!
 * Sobre escritura de la funcion de handler de interrupcion del Timer0
 */
void TIMER0_IRQHandler();

int main(void) {

	SystemInit();
	iniciar_sistema();

	while(1)
	{

	}
	return 0;
}

void iniciar_sistema(){
	PINSEL_CFG_Type entrada_analogica;
	entrada_analogica.Funcnum 	= PINSEL_FUNC_1;
	entrada_analogica.Portnum 	= PINSEL_PORT_0;
	entrada_analogica.Pinnum  	= PINSEL_PIN_23;
	entrada_analogica.Pinmode 	= PINSEL_PINMODE_PULLUP;
	entrada_analogica.OpenDrain = PINSEL_PINMODE_NORMAL;

	PINSEL_ConfigPin(&entrada_analogica);

	PINSEL_CFG_Type salida_digital;
	/*!
	 * LED ROJO
	 */
	salida_digital.Portnum = PINSEL_PORT_0;
	salida_digital.Pinnum  = PINSEL_PIN_18;
	salida_digital.Pinmode = PINSEL_PINMODE_PULLUP;
	salida_digital.Funcnum = PINSEL_FUNC_0;
	salida_digital.OpenDrain = PINSEL_PINMODE_NORMAL;
	PINSEL_ConfigPin(&salida_digital);

	/*!
	 * LED AMARILLO
	 */
	salida_digital.Portnum = PINSEL_PORT_0;
	salida_digital.Pinnum  = PINSEL_PIN_17;
	salida_digital.Pinmode = PINSEL_PINMODE_PULLUP;
	salida_digital.Funcnum = PINSEL_FUNC_0;
	salida_digital.OpenDrain = PINSEL_PINMODE_NORMAL;
	PINSEL_ConfigPin(&salida_digital);

	/*!
	 * LED VERDE
	 */
	salida_digital.Portnum = PINSEL_PORT_0;
	salida_digital.Pinnum  = PINSEL_PIN_16;
	salida_digital.Pinmode = PINSEL_PINMODE_PULLUP;
	salida_digital.Funcnum = PINSEL_FUNC_0;
	salida_digital.OpenDrain = PINSEL_PINMODE_NORMAL;
	PINSEL_ConfigPin(&salida_digital);

	GPIO_SetDir(PORT_ZERO, REDLED, OUTPUT);
	GPIO_SetDir(PORT_ZERO, YELLOWLED, OUTPUT);
	GPIO_SetDir(PORT_ZERO, GREENLED, OUTPUT);

	/****************************************************/
	//				Configuración de ADC
	/****************************************************/
	ADC_Init(LPC_ADC, ADC_MAX_FREQ);

	ADC_ChannelCmd(LPC_ADC, CHANNEL_ZERO, ENABLE);

	ADC_IntConfig(LPC_ADC, CHANNEL_ZERO, ENABLE);

	/****************************************************/
	//				Configuración de Timer0
	/****************************************************/
	TIM_TIMERCFG_Type	timer_0_configuracion;

	timer_0_configuracion.PrescaleOption	    =	TIM_PRESCALE_USVAL;
	timer_0_configuracion.PrescaleValue			=	(uint32_t) 100;

	TIM_Init(LPC_TIM0,TIM_TIMER_MODE,&timer_0_configuracion);

	/****************************************************/
	//				Configuración de Match0
	/****************************************************/

	TIM_MATCHCFG_Type	channel_configuracion;

	channel_configuracion.MatchChannel			=	0;
	channel_configuracion.IntOnMatch			=	ENABLE;
	channel_configuracion.ResetOnMatch			=	ENABLE;
	channel_configuracion.StopOnMatch			=	DISABLE;
	channel_configuracion.ExtMatchOutputType	=	TIM_EXTMATCH_NOTHING;
	channel_configuracion.MatchValue			=	(30*SECOND);

	TIM_ConfigMatch(LPC_TIM0, &channel_configuracion);

	return;
}

void encender_timer0(){
	TIM_Cmd(LPC_TIM0, ENABLE);

	NVIC_EnableIRQ(TIMER0_IRQn);
	return;
}

void ADC_IRQHandler(){
	static uint32_t resultado_conversion = 0;
	//Deshabilitar interrupción de ADC
	NVIC_DisableIRQ(ADC_IRQn);
	for(uint32_t i = 0;i<COUNT_CONVERTION;i++){
		while(ADC_ChannelGetStatus(LPC_ADC,ADC_CHANNEL_0,ADC_DATA_DONE)==RESET);
		resultado_conversion += ADC_GetData(ADC_CHANNEL_0);
		ADC_StartCmd(LPC_ADC, ADC_START_NOW);
	}
	resultado_conversion=resultado_conversion/10;
	analizar_resultado(resultado_conversion);
	return;
}

void analizar_resultado(uint32_t valor_medido){
	if(valor_medido>=80){
	}
	else if(50<=measure && measure<80){
	}
	else if(measure<50){
	}
	return;
}
